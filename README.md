# AngularAuth

Projet angular avec de l'authentification, connectée à une API en Spring Boot

## Nécessaire pour l'authentification
Déjà, pour ce qui est du type d'authentification utilisé dans ce projet, il s'agit de HTTP Basic Auth, qui va utiliser des cookies de sessions et maintenir une session côté API (ce qui fait que techniquement, l'API n'est pas une API "REST", mais plutôt du RCP, vu que le ST de REST veut dire "STateless" et que les sessions sont un state).

Une autre manière assez utilisée pour gérer l'authentification entre un back et un front est avec des JWT. Mais ces derniers sont assez fastidieux à configurer avec Spring Boot.

Côté front, on aura donc besoin :
### Une méthode de connexion
```typescript
login(email:string, password:string) {
    return this.http.get<User>('http://localhost:8080/api/user/account', {
        headers: {
            'Authorization': 'Basic '+btoa(email+':'+password),
        }
    });
}
```
Comme on utilise le HTTP Basic Auth, on doit envoyer les identifiants de connexion directement dans les headers de la requête avec l'email et le mot de passe concaténés et convertis en base64. (Techniquement, n'importe quelle route protégée côté Spring peut faire office de route de connexion, ici, on part sur une route qui nous renvoie aussi les informations du user connecté)

### Une méthode d'inscription
```typescript
register(user:User) {
    return this.http.post<User>('http://localhost:8080/api/user', user);
}
```
Qui va donc poster un nouveau user sur la base de données

### Envoyer et récupérer les cookies
Pour que le HTTP Basic auth avec cookie de session fonctionne avec angular, il faut que celui ci dise à ses requêtes d'envoyer et de récupérer les cookies, pour ça on peut rajouter un `withCredentials:true` sur chaque requête qu'on execute.

Exemple:
```typescript
register(user:User) {
    return this.http.post<User>('http://localhost:8080/api/user', user, {withCredentials:true});
}
```

Mais une manière un peu plus pratique sera de faire ce qu'on appel un interceptor qui est une classe spéciale en angular qui viendra, comme son nom l'indique, intercepter toutes les requêtes faites avec HttpClient et appliquera une modification dessus.

Dans [cet interceptor](src/app/credential.interceptor.ts), on fait fait en sorte de rajouter le withCredentials dans chacune des requêtes qui sera effectuée :

```typescript
intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    request = request.clone({
      setHeaders: {
        'X-Requested-With': 'XMLHttpRequest'
      },
      withCredentials: true
    });
    
    return next.handle(request);
  }
```

On rajoute également un header X-Requested-With pour éviter notamment qu'une requête non authentifiée nous affiche la boîte de dialogue par défaut de login, qui est moche, et qu'on préfèrera gérer entièrement côté front.


### Gérer l'état de connexion
Pour afficher l'interface comme il faut selon l'état de connexion et selon les informations du user connecté, s'il est présent, il faut une manière de stocker quelque part le user en question.

Pour ça, on peut partir sur le fait de stocker le user actuellement connecté directement dans le AuthService
```typescript
export interface AuthState {
  user: User | null;
}

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  state: AuthState = {
    user: null
  };
```
On le stock sous forme d'un objet avec un type AuthState qu'on crée pour l'occasion qui contiendra donc le user.

L'idée sera de faire en sorte que pour chaque requête qui modifie l'état de connexion, on pousse la nouvelle valeur, ou absence de valeur dans ce `state.user`. Pour ce faire, on utilise l'opérateur tap() de rxjs (qui permet d'exécuter une fonction à chaque fois qu'un observable emet une valeur)
```typescript
requeteLogin().pipe(
    tap(data => this.state.user = data) //On pousse le user connectée dans le state
);
```

Pour utiliser ce user dans les templates, il faudra injecter le AuthService dans le component associé, puis assigner le state à une propriété du component ([exemple complet](src/app/home/home.component.ts))

```typescript
export class HomeComponent implements OnInit {
  authState = this.auth.state;

  constructor(private http:HttpClient, private auth:AuthService) { }
```

#### Conserver le user au rechargement
Pour faire en sorte de garder le user connecté si on recharge la page ou si on ferme et réouvre le navigateur, on va faire en sorte de le stocker dans le localStorage (dans le navigateur).

L'idée sera de mettre à jour de user dans le localStorage à chaque fois qu'on exécute une requête qui modifie celui ci (login/register/logout)
```ts
if(data)  {
    localStorage.setItem('user', JSON.stringify(data));
} else {
    localStorage.removeItem('user');
}
```
(Le `JSON.stringify` permet de convertir l'objet user en chaîne de caractères, car le localStorage ne peut stocker que des chaînes de caractères)

Au lancement de l'application, on fera en sorte de récupérer le user actuellement connecté (à l'initialisation du AuthService) pour le stocker dans le service

```ts
constructor(private http: HttpClient) {
    const stored = localStorage.getItem('user');
    if (stored) {
        this.state.user = JSON.parse(stored);
    }
}
```

## Interceptor final (?)
Dans notre [intercepteur d'authentification](src/app/credential.interceptor.ts), on fait en sorte :
* D'envoyer les cookies de session au serveur avec le withCredentials
* De préfixer tous les appels http par l'url de l'api
* De faire que si une requête HTTP renvoie une erreur status 401, supprime le user du AuthService et du localStorage pour indiquer au front qu'on est plus connecté (vu que le front se base sur le localStorage pour savoir s'il est connecté, il peut arriver que le front pense être connecté, alors qu'en fait la session a expiré côté serveur, le front tentera alors de faire une requête vers le back qui échouera et renverra donc un 401 unauthorized. Cette partie de l'intercepteur permet de régler ça)