import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { User } from './entities';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http:HttpClient) { }

  /**
   * 
   * @returns User[] liste de tous les utilisateurs présents sur le serveur
   */
  getAll() {
      return this.http.get<User[]>('/api/admin/user');
  }

  changeRole(id:number, role:string) {
    return this.http.patch<void>('/api/admin/user/'+id+'/role/'+role, null);

  }
}
