import { Component, OnInit } from '@angular/core';
import { AuthService } from './auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent{
  title = 'angular-auth';
  authState = this.auth.state;
  constructor(private auth:AuthService) {}

  logout() {
    this.auth.logout().subscribe();
  }
}
