import { Component, OnInit } from '@angular/core';
import { User } from '../entities';
import { UserService } from '../user.service';

@Component({
  selector: 'app-all-users',
  templateUrl: './all-users.component.html',
  styleUrls: ['./all-users.component.css']
})
export class AllUsersComponent implements OnInit {

  users:User[] = [];

  constructor(private us:UserService) { }

  ngOnInit(): void {
    this.us.getAll().subscribe(data => this.users = data);
  }

  switchRole(user:User) {
    this.us.changeRole(user.id!, user.role!).subscribe();
  }

}
