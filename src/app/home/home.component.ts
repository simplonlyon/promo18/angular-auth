import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { User } from '../entities';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  welcomeMsg = '';
  authState = this.auth.state;

  constructor(private http:HttpClient, private auth:AuthService) { }

  ngOnInit(): void {
    this.http.get<string>('/api/user', {
      responseType: 'text' as 'json'
    }).subscribe(data => this.welcomeMsg = data);

    
  }

}
