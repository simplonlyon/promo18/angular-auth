import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.css']
})
export class ChangePasswordComponent implements OnInit {

  oldPassword = '';
  newPassword = '';
  repeatPassword = '';
  hasError = false;

  constructor(private authService: AuthService, private router: Router) { }

  ngOnInit(): void {
  }

  submitForm() {
    this.hasError = false;
    this.authService.changePassword(this.oldPassword, this.newPassword).subscribe({
      next: () => this.router.navigate(['/']),
      error: () => this.hasError = true
    });
  }



}
