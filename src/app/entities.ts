export interface User {
    id?:number;
    email:string;
    password?:string;
    role?:string;
    dogs?:Dog[];
}

export interface Dog {
    id:number;
    name:string;
    breed:string;
    owner?:User;
}